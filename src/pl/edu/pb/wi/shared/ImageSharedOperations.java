package pl.edu.pb.wi.shared;

import com.sun.istack.internal.NotNull;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageSharedOperations {

    public static final int CHANNEL_RED = 0;
    public static final int CHANNEL_GREEN = 1;
    public static final int CHANNEL_BLUE = 2;
    public static final int CHANNEL_COMBINED = 3;

    public static BufferedImage loadImage(String path) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(path));
        } catch (IOException ex) {
            System.out.println("Error has occured during file reading: " + ex.getMessage());
        }
        return image;
    }

    public static void saveImage(BufferedImage img, String path) {
        try {
            ImageIO.write(img, "jpg", new File(path));
        } catch (IOException ex) {
            System.out.println("Error has occured during file writing: " + ex.getMessage());
        }
    }

    public static BufferedImage convertIconToImage(ImageIcon icon) {
        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics graphics = image.createGraphics();
        icon.paintIcon(null, graphics, 0, 0);
        graphics.dispose();
        return image;
    }

    public static BufferedImage convertImageToGreyscale(BufferedImage image, @NotNull int channel) {
        int red;
        int green;
        int blue;
        int combined;

        for (int w = 0; w < image.getWidth(); w++) {
            for (int h = 0; h < image.getHeight(); h++) {
                Color c = new Color(image.getRGB(w, h));

                switch (channel) {
                    case CHANNEL_RED:
                        red = c.getRed();
                        image.setRGB(w, h, new Color(red, red, red).getRGB());
                        break;
                    case CHANNEL_GREEN:
                        green = c.getGreen();
                        image.setRGB(w, h, new Color(green, green, green).getRGB());
                        break;
                    case CHANNEL_BLUE:
                        blue = c.getBlue();
                        image.setRGB(w, h, new Color(blue, blue, blue).getRGB());
                        break;
                    case CHANNEL_COMBINED:
                        red = c.getRed();
                        green = c.getGreen();
                        blue = c.getBlue();
                        combined = (red + green + blue) / 3;
                        image.setRGB(w, h, new Color(combined, combined, combined).getRGB());
                        break;
                }
            }
        }

        return image;
    }

}
