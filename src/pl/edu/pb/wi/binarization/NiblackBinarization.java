package pl.edu.pb.wi.binarization;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.ArrayList;

import static pl.edu.pb.wi.binarization.Binarization.binarizePixel;

public abstract class NiblackBinarization {

    private static BufferedImage image;
    private static int fieldSize;
    private static double thresholdParameter;

    public static BufferedImage binarizeImage(BufferedImage img, int size, double thresholdParameterByUser) {
        image = img;
        fieldSize = size;
        thresholdParameter = thresholdParameterByUser;

        BufferedImage imageCopy = deepCopy(image);
        int boundary = fieldSize / 2;

        for (int w = boundary; w < image.getWidth() - boundary; w++) {
            for (int h = boundary; h < image.getHeight() - boundary; h++) {

                double threshold = countThreshold(w, h);

                binarizePixel(imageCopy, w, h, threshold);
            }
        }

        return imageCopy;
    }

    private static BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();

        WritableRaster raster = bi.copyData(bi.getRaster().createCompatibleWritableRaster());

        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    private static double countThreshold(int w, int h) {

        ArrayList<Integer> field = getField(w, h);
        int average = countAverage(field);
        double standardDeviation = countStandardDeviation(field);

        return (average + thresholdParameter * standardDeviation);
    }

    private static ArrayList<Integer> getField(int w, int h) {
        ArrayList<Integer> field = new ArrayList<>();
        int color;
        int boundary = fieldSize / 2;

        for (int i = boundary * -1; i <= boundary; i++) {
            for (int j = boundary * -1; j <= boundary; j++) {
                color = new Color(image.getRGB(w + i, h + j)).getRed();
                field.add(color);
            }
        }

        return field;
    }

    private static int countAverage(ArrayList<Integer> field) {
        int sum = field
                .stream()
                .mapToInt(value -> value)
                .sum();

        return sum / field.size();
    }

    private static int countVariation(ArrayList<Integer> field) {
        int average = countAverage(field);
        int sum = field
                .stream()
                .mapToInt(value -> (int) Math.pow(value - average, 2))
                .sum();

        return sum / field.size();
    }

    private static double countStandardDeviation(ArrayList<Integer> field) {
        return Math.sqrt(countVariation(field));
    }
}
