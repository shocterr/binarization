package pl.edu.pb.wi.binarization;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class Binarization {

    public static BufferedImage manualBinarization(BufferedImage img, int threshold) {
        for (int w = 0; w < img.getWidth(); w++) {
            for (int h = 0; h < img.getHeight(); h++) {
                binarizePixel(img, w, h, threshold);
            }
        }
        return img;
    }

    static void binarizePixel(BufferedImage imageCopy, int w, int h, double threshold) {
        Color c = new Color(imageCopy.getRGB(w, h));
        imageCopy.setRGB(w, h,
                c.getRed() >= threshold ? Color.BLACK.getRGB() : Color.WHITE.getRGB());
    }

}
