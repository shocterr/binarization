package pl.edu.pb.wi.viewer;

import pl.edu.pb.wi.binarization.Binarization;
import pl.edu.pb.wi.binarization.NiblackBinarization;
import pl.edu.pb.wi.histogram.HistogramOperations;
import pl.edu.pb.wi.shared.ImageSharedOperations;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;

public class Viewer extends JFrame {

    private final JMenuBar menuBar = new JMenuBar();
    private final JMenu files = new JMenu("File");
    private final JMenu histogram = new JMenu("Histograms");
    private final JMenu binarization = new JMenu("Binarization");
    private final JMenuItem loadImage = new JMenuItem("Load image");
    private final JMenuItem saveImage = new JMenuItem("Save image");
    private final JMenuItem convertToGreyscaleRed = new JMenuItem("Convent image to greyscale based on red channel");
    private final JMenuItem convertToGreyscaleGreen = new JMenuItem("Convent image to greyscale based on green channel");
    private final JMenuItem convertToGreyscaleBlue = new JMenuItem("Convent image to greyscale based on blue channel");
    private final JMenuItem convertToGreyscaleCombined = new JMenuItem("Convent image to greyscale based on combined channels");
    private final JMenuItem niblackBinarization = new JMenuItem("Niblack binarization");
    private final JMenuItem manualBinarization = new JMenuItem("Manual binarization");
    private final JMenuItem calculateHistograms = new JMenuItem("Calculate histograms");
    private final JLabel imageLabel = new JLabel();

    public Viewer() {
        this.setLayout(new BorderLayout());
        this.setTitle("Podstawy Biometrii");
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);

        this.menuBar.add(this.files);
        this.menuBar.add(this.histogram);
        this.menuBar.add(this.binarization);
        this.binarization.add(niblackBinarization);
        this.binarization.add(manualBinarization);
        this.files.add(this.loadImage);
        this.files.add(this.saveImage);
        this.files.add(this.convertToGreyscaleRed);
        this.files.add(this.convertToGreyscaleGreen);
        this.files.add(this.convertToGreyscaleBlue);
        this.files.add(this.convertToGreyscaleCombined);
        this.histogram.add(this.calculateHistograms);

        this.add(this.menuBar, BorderLayout.NORTH);
        this.add(this.imageLabel, BorderLayout.CENTER);
        this.imageLabel.setHorizontalAlignment(JLabel.CENTER);
        this.imageLabel.setVerticalAlignment(JLabel.CENTER);

        this.loadImage.addActionListener((ActionEvent e) -> {
            JFileChooser imageOpener = new JFileChooser();
            imageOpener.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    String fileName = f.getName().toLowerCase();
                    if(fileName.endsWith(".jpg") || fileName.endsWith(".png")
                            || fileName.endsWith(".tiff") || fileName.endsWith(".jpeg")) {
                        return true;
                    } else return false;
                }

                @Override
                public String getDescription() {
                    return "Image files (.jpg, .png, .tiff)";
                }
            });

            int returnValue = imageOpener.showDialog(null, "Select image");
            if(returnValue == JFileChooser.APPROVE_OPTION) {
                BufferedImage img = ImageSharedOperations.loadImage(imageOpener.getSelectedFile().getPath());
                this.imageLabel.setIcon(new ImageIcon(img));
            }
        });

        this.saveImage.addActionListener((ActionEvent e) -> {
            String path = "./image.jpg";
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            ImageSharedOperations.saveImage(img, path);
        });

        this.calculateHistograms.addActionListener((ActionEvent e) -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            HistogramOperations.calculateHistograms(img);
        });

        this.convertToGreyscaleRed.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            BufferedImage convertedImage = ImageSharedOperations.convertImageToGreyscale(img,
                    ImageSharedOperations.CHANNEL_RED);
            imageLabel.setIcon(new ImageIcon(convertedImage));
        });

        this.convertToGreyscaleGreen.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            BufferedImage convertedImage = ImageSharedOperations.convertImageToGreyscale(img,
                    ImageSharedOperations.CHANNEL_GREEN);
            imageLabel.setIcon(new ImageIcon(convertedImage));
        });

        this.convertToGreyscaleBlue.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            BufferedImage convertedImage = ImageSharedOperations.convertImageToGreyscale(img,
                    ImageSharedOperations.CHANNEL_BLUE);
            imageLabel.setIcon(new ImageIcon(convertedImage));
        });

        this.convertToGreyscaleCombined.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            BufferedImage convertedImage = ImageSharedOperations.convertImageToGreyscale(img,
                    ImageSharedOperations.CHANNEL_COMBINED);
            imageLabel.setIcon(new ImageIcon(convertedImage));
        });

        this.niblackBinarization.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());

            int fieldSize = Integer.parseInt(JOptionPane.showInputDialog(this,
                    "Enter field size (odd number)"));

            double thresholdParameter = Double.parseDouble(JOptionPane.showInputDialog(this,
                    "Enter threshold parameter"));


            BufferedImage binarizedImage = NiblackBinarization.binarizeImage(img, fieldSize, thresholdParameter);
            imageLabel.setIcon(new ImageIcon(binarizedImage));
        });

        this.manualBinarization.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());

            int thresholdParameter = Integer.parseInt(JOptionPane.showInputDialog(this,
                    "Enter threshold parameter"));

            BufferedImage binarizedImage = Binarization.manualBinarization(img, thresholdParameter);
            imageLabel.setIcon(new ImageIcon(binarizedImage));
        });
        
    }

}
